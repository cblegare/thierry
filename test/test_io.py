from textwrap import dedent

import pytest

from outil import io


def test_yaml_configuration():
    data = {
        "top scalar": "top scalar value",
        "top map": {"map scalar": "map scalar value"},
        "top list": ["list value"],
    }
    expected = dedent(
        """\
    top scalar: top scalar value
    top map:
      map scalar: map scalar value
    top list:
      - list value
    """
    )
    actual = io.yaml.dumps(data)
    assert actual == expected


@pytest.mark.parametrize(
    "test_string,expected",
    [
        ("aA", "a_a"),
        ("aAa", "a_aa"),
        ("snake2_snake", "snake_2_snake"),
        ("httpRequest", "http_request"),
        ("HTTPRequest", "http_request"),
        ("getHTTPResponseCode", "get_http_response_code"),
        ("get200HTTPResponseCode", "get_200_http_response_code"),
        ("getHTTP200ResponseCode", "get_http_200_response_code"),
        ("ResponseHTTP2", "response_http_2"),
        ("ResponseHTTP2", "response_http_2"),
        ("10CoolDudes", "10_cool_dudes"),
        ("Camel2WARNING_Case_CASE", "camel_2_warning_case_case"),
    ],
)
def test_snake_case(test_string, expected):
    assert io.snake_case(test_string) == expected
