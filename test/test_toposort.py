import pytest

from outil.toposort import flat_topological_sort, topological_sort

objects = [object() for _ in range(12)]

valid_graphs = {
    "empty": {
        "graph": {},
        "set_result": [],
        "flat_result": [],
    },
    "trivial": {"graph": {1: set()}, "set_result": [{1}], "flat_result": [1]},
    "simple_no_deps": {
        "graph": {
            1: set(),
            2: set(),
            3: set(),
        },
        "set_result": [{2, 3, 1}],
        "flat_result": [1, 2, 3],
    },
    "trivial_deps": {
        "graph": {1: {1}},
        "set_result": [{1}],
        "flat_result": [1],
    },
    "trivial_orphan_deps": {
        "graph": {1: {2}},
        "set_result": [{2}, {1}],
        "flat_result": [2, 1],
    },
    "orphan_deps": {
        "graph": {1: {2}, 3: {4}},
        "set_result": [{4, 2}, {3, 1}],
        "flat_result": [2, 4, 1, 3],
    },
    "integers": {
        "graph": {
            2: {11},
            9: {11, 8},
            10: {11, 3},
            11: {7, 5},
            8: {7, 3},
        },
        "set_result": [
            {3, 5, 7},
            {8, 11},
            {2, 9, 10},
        ],
        "flat_result": [3, 5, 7, 8, 11, 2, 9, 10],
    },
    "strings": {
        "graph": {
            "02": {"11"},
            "09": {"11", "08"},
            "10": {"11", "03"},
            "11": {"07", "05"},
            "08": {"07", "03"},
        },
        "set_result": [
            {"03", "05", "07"},
            {"08", "11"},
            {"02", "09", "10"},
        ],
        "flat_result": ["03", "05", "07", "08", "11", "02", "09", "10"],
    },
}


@pytest.mark.parametrize(
    ["graph", "expected"],
    [
        (definition["graph"], definition["set_result"])
        for definition in valid_graphs.values()
    ],
    ids=[name for name in valid_graphs.keys()],
)
def test_sort(graph, expected):
    actual = [item for item in topological_sort(graph)]
    assert actual == expected


@pytest.mark.parametrize(
    ["graph", "expected"],
    [
        (definition["graph"], definition["flat_result"])
        for definition in valid_graphs.values()
    ],
    ids=[name for name in valid_graphs.keys()],
)
def test_flat_sort(graph, expected):
    actual = [item for item in flat_topological_sort(graph)]
    assert actual == expected
