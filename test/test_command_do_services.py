import inspect
from typing import Callable, Type

import pytest
import typeguard

from outil.app.command import Command
from outil.app.services import all_commands, all_definitions


@pytest.mark.parametrize("command_cls", all_commands())
def test_command_do_is_resolvable(command_cls: Type[Command], services):
    command_signature = inspect.signature(command_cls._do)

    for service_name, parameter in command_signature.parameters.items():
        if service_name == "self":
            continue
        service_factory = services[service_name]
        expected_service_type = parameter.annotation

        typeguard.check_type(
            service_name,
            service_factory,
            Callable[[...], expected_service_type],
        )


@pytest.fixture()
def services():
    return all_definitions()
