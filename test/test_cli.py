import os
from pathlib import Path

from click.testing import CliRunner

from outil import cli
from outil.template.setuptools import DOTFILES_ENTRYPOINT_GROUP


def test_outil():
    runner = CliRunner()
    result = runner.invoke(cli.app)
    assert result.exit_code == 0
    assert "not really" + os.linesep in result.output, result.output


def test_outil_dotfiles(tmpdir):
    runner = CliRunner()
    home_dir = Path(tmpdir)
    dotfiles_dir = home_dir.joinpath(".config", "dotfiles")

    result = runner.invoke(
        cli.app,
        [
            "dotfiles",
            "--dotfiles-dir",
            str(dotfiles_dir.parent),
            "--home",
            str(home_dir),
        ],
    )
    assert result.exit_code == 0
    assert "not really" not in result.output, result.output
    assert home_dir.joinpath(".bashrc").is_symlink()
    assert home_dir.joinpath(".bashrc").resolve() == dotfiles_dir.joinpath(
        "bashrc"
    )


def test_group_matches_installation(setup_config):
    entry_points = list(setup_config["options.entry_points"])
    assert DOTFILES_ENTRYPOINT_GROUP in entry_points
