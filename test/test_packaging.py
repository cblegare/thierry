import pkg_resources
import pytest

DIST_NAME = "outil"


def test_all_extra_contains_all_extras(setup_config, current_distribution):
    """
    Ensure that an `all` extra exists that contains all the dependencies
    from other extras.

    Note:

        This test along with the `all` extra should be removed if something
        like support for globbing syntax when specifying extras with `pip`.

        Related `pip` issue: https://github.com/pypa/pip/issues/5039.
    """
    extras = setup_config["options.extras_require"]

    all_extras = set()
    expected = set()

    for extra_name, dependencies in extras.items():
        if extra_name == "all":
            all_extras.update(dependencies.strip().splitlines())
        else:
            expected.update(dependencies.strip().splitlines())

    assert all_extras == expected


def test_extras_installed(setup_config, current_distribution):
    """
    This test ensures that configured extras dependencies are properly
    managed by :mod:`setuptools`.
    """
    extras = setup_config["options.extras_require"]
    assert set(extras.keys()) == set(current_distribution.extras)


def test_entrypoints_match_installation(
    entry_point_definitions, current_distribution
):
    assert set(entry_point_definitions.keys()) == set(
        current_distribution.get_entry_map()
    ), (
        "Configured entry points do not match installation. "
        "Check the setup.cfg file or reinstall the distribution "
        "you are testing against."
    )


def test_default_entrypoint_found(
    default_entry_point_src,
    templates_entrypoints,
    default_template_name,
    dotfiles_templates_entry_point_group,
):

    assert default_entry_point_src in templates_entrypoints, (
        'The "{!s}" entry point in group "{!s}" '
        "is not found in the distribution configuration".format(
            default_template_name, dotfiles_templates_entry_point_group
        )
    )


def test_entrypoint_initialization(
    current_distribution,
    default_entry_point_src,
    default_template_module,
    default_template_name,
):
    entry_point = pkg_resources.EntryPoint.parse(
        default_entry_point_src, dist=current_distribution
    )

    assert entry_point.module_name == default_template_module
    assert entry_point.name == default_template_name
    assert entry_point.load() is not None


@pytest.fixture()
def current_distribution():
    return pkg_resources.get_distribution(DIST_NAME)


@pytest.fixture()
def dotfiles_templates_entry_point_group():
    return "outil.templates"


@pytest.fixture()
def default_template_name():
    return "dotfiles"


@pytest.fixture()
def default_template_module():
    return f"{DIST_NAME}.templates.dotfiles"


@pytest.fixture()
def default_entry_point_src(default_template_name, default_template_module):
    return f"{default_template_name} = {default_template_module}"


@pytest.fixture()
def entry_point_definitions(setup_config):
    return setup_config["options.entry_points"]


@pytest.fixture()
def templates_entrypoints(
    entry_point_definitions, dotfiles_templates_entry_point_group
):
    return entry_point_definitions[dotfiles_templates_entry_point_group]
