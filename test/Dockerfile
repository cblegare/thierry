FROM ubuntu:18.04
MAINTAINER Charles Bouchard-Légaré <cblegare.atl@ntis.ca>

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive \
    DEBCONF_NONINTERACTIVE_SEEN=true \
    apt-get install -y \
      git \
      zsh \
      make build-essential llvm \
      libssl-dev zlib1g-dev libbz2-dev libreadline-dev \
      libsqlite3-dev libncurses5-dev xz-utils tk-dev \
      libxml2-dev libxmlsec1-dev libffi-dev \
      wget curl \
    && apt-get clean

RUN useradd -ms /bin/zsh tester
RUN mkdir -p /etc/sudoers.d/ && \
    chown -R tester:tester /home/tester && \
    echo 'tester ALL=(ALL) NOPASSWD:ALL' > /etc/sudoers.d/tester && \
    chmod 0440 /etc/sudoers.d/tester
USER tester

ENV PYENV_ROOT /home/tester/.pyenv
ENV PATH /home/tester/.pyenv/shims:/home/tester/.pyenv/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
RUN curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | zsh
RUN pyenv install 3.5.6 && \
    pyenv install 3.6.5 && \
    pyenv global 3.6.5

ADD . /repo
WORKDIR /repo
