Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog`_, and this project adheres to
`Semantic Versioning`_.

..
    How do I make a good changelog?
    ===============================

    Guiding Principles
    ------------------

    - Changelogs are for humans, not machines.
    - There should be an entry for every single version.
    - The same types of changes should be grouped.
    - Versions and sections should be linkable.
    - The latest version comes first.
    - The release date of each version is displayed.
    - Mention whether you follow Semantic Versioning.

    Types of changes
    ----------------

    - **Added** for new features.
    - **Changed** for changes in existing functionality.
    - **Deprecated** for soon-to-be removed features.
    - **Removed** for now removed features.
    - **Fixed** for any bug fixes.
    - **Security** in case of vulnerabilities.

    [1.0.0] - 2017-06-20
    --------------------

    Added
    ~~~~~

    - Added a feature.


[Unreleased]
------------

Added
~~~~~

- Load a Cookiecutter_ template from an Entrypoint_: `!1`_.
- Mimic some of rcm_ behaviors.
- Add some nice defaults

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _Entrypoint: https://setuptools.readthedocs.io/en/latest/pkg_resources.html#entry-points
.. _`!1`: https://gitlab.com/cblegare/thierry/merge_requests/1
.. _rcm: https://github.com/thoughtbot/rcm

.. _Keep a Changelog: https://keepachangelog.com/en/1.0.0/
.. _Semantic Versioning: https://semver.org/spec/v2.0.0.html
