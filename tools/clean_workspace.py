from collections import OrderedDict, defaultdict
import fnmatch
import os
from pathlib import Path

import click


WORKSPACE_ROOT = Path(__file__).parent.parent


clean_patterns = [
    './build',
    './dist',
    './.env*',
    './.pytest_cache*',
    './src/*/__pycache__',
    '*.egg-info',
    './.tox',
    './doc/thierry.*.rst',
]


_clean_patterns = {
    'build': ['./build',
              './dist',
              './doc/thierry.*.rst',
              '*.egg-info'],
    'cache': ['./src/*/__pycache__',
              ],
    'test': ['./.env*',
             './.pytest_cache*',
             './.tox',],
    'venv': []
}


keep_patterns = [
    '*.py',
]


@click.command()
def main():
    criteria = make_criteria(keep=keep_patterns,
                             clean=clean_patterns)
    t = Tree(WORKSPACE_ROOT.resolve().name)
    for root, dirs, filenames in os.walk(WORKSPACE_ROOT):
        if criteria(root):
            t.add_child(root)
            continue
        for filename in sorted(filenames):
            path = Path(root, filename)
            if criteria(path):
                t.add_child(path)

    for i in t.traverse():
        print(i.displayable())


class Node(object):
    display_filename_prefix_middle = '├──'
    display_filename_prefix_last = '└──'
    display_parent_prefix_middle = '    '
    display_parent_prefix_last = '│   '

    def __init__(self, name, parent, root_or_tree):
        self.name = str(name)
        self.parent = parent
        self.tree = root_or_tree

    def __str__(self):
        return str(self.name)

    def add_child(self, path):
        path = str(path)
        return self.tree.add_child('{!s}/{!s}'.format(self.name, path))

    @property
    def children(self):
        for child_name in sorted(self.tree.by_parent[self.name]):
            yield self.tree.nodes[child_name]

    @property
    def _is_last(self):
        return list(self.parent.children)[-1] == self

    @property
    def displayname(self):
        return self.name.split('/')[-1]

    def traverse(self):
        yield self
        for child in self.children:
            yield from child.traverse()

    def displayable(self):
        if self.parent is None:
            return self.displayname

        _filename_prefix = (self.display_filename_prefix_last
                            if self._is_last
                            else self.display_filename_prefix_middle)

        parts = ['{!s} {!s}'.format(_filename_prefix,
                                    self.displayname)]

        parent = self.parent
        while parent and parent.parent is not None:
            parts.append(self.display_parent_prefix_middle
                         if parent._is_last
                         else self.display_parent_prefix_last)
            parent = parent.parent

        return ''.join(reversed(parts))


class Tree(Node):
    def __init__(self, root):
        super().__init__(root, None, self)
        self.nodes = OrderedDict()
        self.by_parent = defaultdict(OrderedDict)

    def add_child(self, path):
        path = str(path)
        parts = []
        parent = self
        for part in path.lstrip('.').lstrip('/').split('/'):
            parts.append(part)
            child_name = '/'.join(parts)
            if child_name not in self.nodes:
                child = Node(child_name, parent, self)
                self.nodes[child_name] = child
                self.by_parent[parent.name][child_name] = child_name
            else:
                child = self.nodes[child_name]
            parent = child
        return parent


def make_criteria(keep=None, clean=None):
    keep = keep or []
    clean = clean or []

    def _matcher(path):

        for pattern in keep:
            if fnmatch.fnmatch(str(path), pattern):
                return False

        for pattern in clean:
            if fnmatch.fnmatch(str(path), pattern):
                return True
        else:
            return False

    return _matcher


if __name__ == '__main__':
    main()
