"""
###################
|outil| application
###################

This package provides primitives for wiring, resolving and instanciating
components of an application.

This package is where |outil| really feels like an **application** and not
a **library**.
"""
