"""
####################
Workspace management
####################

Find your local repos, but only if they're easy to find!
"""
from pathlib import Path
from typing import List


class Workspace:
    def find_project(self, name: str) -> Path:
        raise NotImplementedError

    def find_projects(self, name: str) -> List[Path]:
        raise NotImplementedError
