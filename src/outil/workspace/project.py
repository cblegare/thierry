"""
########
Projects
########

What gives?
"""

from collections import deque
from pathlib import Path
from typing import Deque, Iterator, List

from outil.input import ChoicePrompt
from outil.workspace import Workspace


class GitRepositories(Workspace):
    def __init__(self, root: Path, choice_prompt: ChoicePrompt[Path]):
        self.root = root
        self._prompt = choice_prompt

    def find_project(self, name: str) -> Path:
        found = self.find_projects(name)

        if not found:
            raise RuntimeError(f"No project named '{name}' could be found")
        elif len(found) > 1:
            choice = self._prompt(found)
            if choice is None:
                raise RuntimeError("Could not choose project")
            else:
                return choice
        else:
            return found[0]

    def find_projects(self, name: str) -> List[Path]:
        return [
            folder
            for folder in gen_repositories(self.root)
            if folder.name == name
        ]


def gen_repositories(root: Path) -> Iterator[Path]:
    """
    Iterate on git repositories found at root.

    A folder is considered a git repositories if it has a '.git' folder
    at its root.

    Repositories found are not searched further, hence git submodules
    are not discovered by this function.

    Args:
        root: Directory where to start looking for repositories.

    Returns:
        Folders that look like git repositories.
    """
    stack: Deque[Path] = deque()
    stack.append(root)
    while stack:
        folder = stack.pop()
        if folder.joinpath(".git").is_dir():
            yield folder
        else:
            stack.extend(
                subfolder
                for subfolder in folder.iterdir()
                if subfolder.is_dir()
            )
