"""
###############
Terminal editor
###############

This module thinly wraps my editor of choice.
"""

import subprocess


class Editor:
    def __init__(self, *args: str) -> None:
        self._args = args

    def start(self) -> None:
        cmd = ["nvim"]
        cmd.extend([str(arg) for arg in self._args])
        subprocess.run(cmd)
