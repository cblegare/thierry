"""
#######################
Management of dotfiles.
#######################

This is a shameful clone of rcm_, a management suite for dotfiles.

.. _rcm: https://github.com/thoughtbot/rcm
"""
import logging
import os
import subprocess
from pathlib import Path
from typing import Iterator, Union

log = logging.getLogger(__name__)


def up(source: Union[str, Path], destination: Union[str, Path]) -> Path:
    """
    Update and install managed dotfiles.

    This is a shameful clone of rcup_.

    For more information, see :meth:`~Dotfiles.up`.
    """
    return Dotfiles(Path(source), Path(destination)).up()


class Dotfiles:
    """
    Dotfiles!

    Args:
        source: Initialize :attr:`~source`.
        destination: Initialize :attr:`~destination`.
    """

    def __init__(self, source: Path, destination: Path):
        self._destination = destination
        self._source = source

    @property
    def destination(self) -> Path:
        """
        The destination where to apply dotfiles.

        Returns:
            This is usually is usually a home directory as
                determined by the ``HOME`` environment variable.
        """
        return self._destination

    @property
    def source(self) -> Path:
        """
        The managed dotfiles location.

        Returns:
            This is usually something like ``~/.dotfiles``.
        """
        return self._source

    def up(self) -> Path:
        """
        Update and install managed dotfiles.

        This is a shameful clone of rcup_.

        It is instructive to understand the process :func:`~up` uses when
        synchronizing your rc files:

        #.  The pre-up hook is run.

        #.  All non-host, non-tag files without a dot prefix are symlinked to
            the dotted filename in your home directory. So, ``.dotfiles/tigrc``
            is symlinked to ``~/.tigrc``.

        #.  All non-host, non-tag directories have their structure copied to
            your home directory, then a non-dotted symlink is created within.
            So for example, ``.dotfiles/vim/autoload/haskell.vim`` causes the
            ``~/.vim/autoload`` directory to be created, then ``haskell.vim``
            is symlinked within.

        #.  Steps (2) and (3) are applied to host-specific files. These are
            files under a directory named host-$HOSTNAME.

        #.  Steps (2) and (3) are applied to tag-specific files. These are
            files under directories named tag-$TAG_NAME, where $TAG_NAME is the
            name of each specified tag in turn, taken from the command line or
            from rcrc(5).

        #.  The post-up hook is run.

        .. _rcup: http://thoughtbot.github.io/rcm/rcup.1.html
        """
        for dotfile in self._up_dotfiles():
            pass

        return self._destination

    def _symlink_top_dotfiles(self) -> Iterator[Path]:
        log.debug("symlinking top dotfiles")

        yield from (
            self._ln_s(
                top_dotfile, self.destination.joinpath("." + top_dotfile.name)
            )
            for top_dotfile in self._top_dotfiles(self.source)
        )

    def _mirror_dotdirs_structure(self) -> Iterator[Path]:
        log.debug("mirroring dotfiles directory structure")

        def dotdir_name(source: Path) -> Path:
            top_dotdir, *rest = source.relative_to(self.source).parts
            newname = self.destination.joinpath("." + top_dotdir, *rest)
            return newname

        yield from (
            self._mkdir_p(dotdir_name(leaf_directory))
            for leaf_directory in self._dotdir_leaves(self.source)
        )

    def _symlink_sub_dotfiles(self) -> Iterator[Path]:
        log.debug("symlinking deep dotfiles")

        def dotdir_name(source: Path) -> Path:
            top_dotdir, *rest = source.relative_to(self.source).parts
            newname = self.destination.joinpath("." + top_dotdir, *rest)
            return newname

        yield from (
            self._ln_s(sub_dotfile, dotdir_name(sub_dotfile))
            for sub_dotfile in self._sub_dotfiles(self.source)
        )

    def _up_dotfiles(self) -> Iterator[Path]:
        self._run_hook(self.source / "hooks" / "pre-up")
        yield from self._symlink_top_dotfiles()
        yield from self._mirror_dotdirs_structure()
        yield from self._symlink_sub_dotfiles()
        self._run_hook(self.source / "hooks" / "post-up")

    def _run_hook(self, path: Path) -> None:
        for executable in self._executables(path):
            log.debug("running hook {!s}".format(executable))
            process = subprocess.run(
                [str(executable)],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
            )
            if process.returncode:
                log.error('Hook "{!s}" failed.'.format(executable))
                log.error("  stdout: {!s}".format(process.stdout))
                log.error("  stderr: {!s}".format(process.stderr))

    @classmethod
    def _executables(cls, path: Path) -> Iterator[Path]:
        """
        Provide executables at path.

        This is inspired by the same principle behind abstraction of the
        `conf` file vs `conf.d` folders, except without the directory name
        suffix.

        There are three possibilities to consider:

        #.  `path` is an executable file, then is returned alone in a tuple

        #.  `path` is a directory containing executable files which are returned
            as a generator

        #.  `path` is something else and an empty tuple is returned

        Raises
            NotImplementedError: This may not be supported under some non-Unix
                systems. See :func:`os.access` for details.

        Args:
            path: A :term:`path-like` object to check for executables.

        Returns:
            Something you can iterate on that contains presumably executable
                file paths.

        .. _path-like: https://docs.python.org/3/glossary.html#term-path-like-object
        """
        if path.is_dir():
            return (
                executable
                for executable in sorted(path.glob("*"))
                if os.access(str(executable), os.X_OK)
            )
        elif os.access(str(path), os.X_OK):
            yield from (path,)
        else:
            return ()

    @classmethod
    def _gen_dotfiles(cls, source: Path) -> Iterator[Path]:
        """
        Generate valid dotfiles under the `source` directory.

        Note:

            This method does not recurse in child directories.

        Args:
            source: Where dotfiles are

        Returns:
            Something you can iterate on that contains presumably dotfiles.
        """
        yield from sorted(
            dotfile
            for dotfile in source.glob("*")
            if cls._looks_like_valid_dotfile(dotfile)
        )

    @classmethod
    def _looks_like_valid_dotfile(cls, path: Path) -> bool:
        return not (
            path.name.startswith(".")
            or path.name == "hooks"
            or path.name.startswith("tag")
            or path.name.startswith("host")
        )

    @classmethod
    def _dotdir_leaves(cls, dotfiles_dir: Path) -> Iterator[Path]:
        yield from (
            Path(leaf_dir)
            for dotfile in cls._gen_dotfiles(dotfiles_dir)
            if dotfile.is_dir()
            for leaf_dir, subs, _ in sorted(os.walk(str(dotfile)))
            if not subs
        )

    @classmethod
    def _top_dotfiles(cls, dotfiles_dir: Path) -> Iterator[Path]:
        yield from (
            dotfile
            for dotfile in cls._gen_dotfiles(dotfiles_dir)
            if dotfile.is_file()
        )

    @classmethod
    def _sub_dotfiles(cls, dotfiles_dir: Path) -> Iterator[Path]:
        yield from (
            Path(leaf_dir, filename)
            for dotfile in cls._gen_dotfiles(dotfiles_dir)
            if dotfile.is_dir()
            for leaf_dir, subs, filenames in sorted(os.walk(str(dotfile)))
            for filename in filenames
        )

    @classmethod
    def _ln_s(cls, target: Path, link: Path) -> Path:
        try:
            log.debug("linking {!s} -> {!s}".format(link, target))
            link.symlink_to(target)
        except FileExistsError:
            backup = link.with_name(link.name + ".backup")
            log.info(
                "file {!s} exists: keeping backup at {!s}"
                "".format(link, backup)
            )
            link.rename(backup)
            link.symlink_to(target)
        finally:
            return link

    @classmethod
    def _mkdir_p(cls, path: Path) -> Path:
        log.debug("making dir {!s}".format(path))
        path.mkdir(parents=True, exist_ok=True)
        return path
