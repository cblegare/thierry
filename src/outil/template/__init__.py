"""
##########################
File and folder templating
##########################

Rendering files is like attracting anemones: a must-have for your shell.
"""


import logging
import shutil
import tempfile
from pathlib import Path
from typing import Dict, Optional

from outil.app.command import Command
from outil.io import FolderComparison

log = logging.getLogger(__name__)


class Template:
    """
    Render a bunch of files
    """

    def render(
        self,
        parent_folder: Path,
        data: Optional[Dict[str, str]] = None,
    ) -> Path:
        """
        Renders a templated folder to the filesystem.

        Args:
            parent_folder: The folder in which the rendered folder will be written.
            data: Values to be replaced in the template.

        Returns:
            The rendered folder name.
        """
        pass


Templates = Dict[str, Template]


class CautiousTemplate(Template):
    def __init__(self, wrapped: Template):
        self._wrapped = wrapped

    def render(
        self,
        parent_folder: Path,
        data: Optional[Dict[str, str]] = None,
    ) -> Path:
        parent_folder.mkdir(parents=True, exist_ok=True)
        with tempfile.TemporaryDirectory() as tmpdirname:
            tmp_parent_folder = Path(tmpdirname, parent_folder.name)
            rendered = self._wrapped.render(tmp_parent_folder, data)

            actual_destination = parent_folder.joinpath(rendered.name)

            if actual_destination.exists():
                comparison = FolderComparison(actual_destination, rendered)

                for line in comparison.reportlines:
                    log.info(line.strip())
                if comparison.destructive:
                    raise RuntimeError("Render would make destructive changes.")
                shutil.rmtree(parent_folder)

            shutil.move(rendered, actual_destination)

        return actual_destination


class New(Command):
    """
    Render files from a template.
    """

    def __init__(
        self, template_name: str, parent_destination: Path, data: Dict[str, str]
    ):
        self.template_name = template_name
        self.parent_folder = parent_destination
        self.data = data

    def _do(self, templates: Templates) -> Path:  # type: ignore
        template = templates[self.template_name]
        rendered_folder = template.render(self.parent_folder, data=self.data)
        return rendered_folder


class Dotfiles(Command):
    def __init__(
        self,
        template_name: str,
        dotfiles_dir: Path,
        home: Path,
        data: Dict[str, str],
    ):
        self.template_name = template_name
        self.dotfiles_dir = dotfiles_dir
        self.home = home
        self.data = data

    def _do(self, templates: Templates) -> Path:  # type: ignore
        from outil.template.rcm import up

        template = templates[self.template_name]
        rendered_folder = template.render(self.dotfiles_dir, data=self.data)
        destination = up(rendered_folder, self.home)

        return destination
