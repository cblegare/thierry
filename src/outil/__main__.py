"""
############################
Top-level script environment
############################

Make sure one can invoke !outil! with ``python -m outil``.
"""
import sys

from outil import __dist__
from outil.cli import app

if __name__ == "__main__" and not __package__:
    # This should never happen when installed from pip.
    # This workaround is NOT bulletproof, rather brittle as many edge
    # cases are not covered
    # See http://stackoverflow.com/a/28154841/2479038

    print(
        "warning: running package directly, risking ImportError",
        file=sys.stderr,
    )


if __name__ == "__main__":
    app(prog_name=__dist__.project_name)
