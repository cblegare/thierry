"""
#####################
|outil| configuration
#####################

At some point configuration will be useful, right? Right?!?
"""


class Config:
    def __init__(
        self,
        dry_run: bool = False,
    ):
        self.dry_run = dry_run
