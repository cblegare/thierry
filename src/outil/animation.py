"""
##########################################
The mascot made me code something for them
##########################################

This module holds and animation of Thierry.

An animation? Who is Thierry?! What is this nonsense?!?
"""

walk_right = [
    r"""
 __
(__)_
(____)_
(______)
./(  00 )\\
""",
    r"""
  __
 (__)_
 (____)_
 (______)
.//(  00 )\
""",
]
walk_left = [
    r"""
        __
      _(__)
    _(____)
   (______)
//( 00  )\.
""",
    r"""
       __
     _(__)
   _(____)
  (______)
/( 00  )\\.
""",
]


def run_animation(width: int, fps: float) -> None:
    import os
    import subprocess
    import time

    delay = 1.0 / fps
    offset = 0

    positions = [(i, walk_right) for i in range(width - 11)] + [
        (i, walk_left) for i in reversed(range(width - 11))
    ]

    def render_frame(frame: str, offset: int) -> None:
        subprocess.call("cls" if os.name == "nt" else "clear", shell=True)
        lines = frame.splitlines()
        for i, line in enumerate(lines):
            if i != len(lines) - 1:
                print(" " * offset + line)
            else:
                print(
                    "." * offset + line + "." * (width - (offset + len(line)))
                )

    for offset, animation in positions:
        for frame in animation:
            render_frame(frame, offset)
            time.sleep(delay)


if __name__ == "__main__":
    run_animation(36, 24)
