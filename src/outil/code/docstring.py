from typing import Any


def docstring_summary(obj: Any) -> str:
    """
    Extract the docstring summary from any object.

    Args:
        obj: Any python object.
        default: Default value if not summary could be extracted.

    Returns:

    """
    doc = obj.__doc__ or ""
    for line in doc.splitlines():
        stripped = line.strip()
        if stripped:
            return stripped
    else:
        return ""
