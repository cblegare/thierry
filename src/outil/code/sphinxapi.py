from __future__ import annotations

import os
import logging
import sys
from pathlib import Path
import subprocess
from typing import Callable, Dict, List, Optional, Union, Any

from outil.error import DependencyMissing
from outil.io import BaseModel

log = logging.getLogger(__name__)


def get_sphinx() -> Callable[[List[str]], int]:
    try:
        import sphinx.cmd.build
    except ImportError as e:
        log.critical("Sphinx could not be imported")
        log.info("You can install it with")
        log.info("  pip install sphinx")
        raise DependencyMissing("sphinx") from e

    return sphinx.cmd.build.main


def get_apidoc() -> Callable[[List[str]], int]:
    try:
        import sphinx.ext.apidoc
    except ImportError as e:
        log.critical("Sphinx could not be imported")
        log.info("You can install it with")
        log.info("  pip install sphinx")
        raise DependencyMissing("sphinx") from e

    return sphinx.ext.apidoc.main


class SphinxProject(BaseModel):
    doc_root: Path
    build_root: Path


class SphinxBuildOptions(BaseModel):
    write_all: bool = False
    """
    If given, always write all output files.
    The default is to only write output files for new and changed source files.
    (This may not apply to all builders.)

    See :option:`sphinx:sphinx-build -a`.
    """

    rebuild_env: bool = False
    """
    Don’t use a saved environment (the structure caching all cross-references),
    but rebuild it completely. The default is to only read and parse source
    files that are new or have changed since the last run.

    See :option:`sphinx:sphinx-build -E`.
    """

    conf_dir: Optional[Path] = None
    """
    Don’t look for the conf.py in the source directory, but use the given
    configuration directory instead. Note that various other files and paths
    given by configuration values are expected to be relative to the
    configuration directory, so they will have to be present at this location
    too.

    See :option:`sphinx:sphinx-build -c`.
    """

    noconfig: bool = False
    """
    Don't look for a configuration file; only take options via the
    :attr:`~settings` option.

    See :option:`sphinx:sphinx-build -C`.
    """

    tags: Optional[List[str]] = None
    """
    Define the tags. This is relevant for :rst:dir:`sphinx:only` directives
    that only include their content if this tag is set.

    See :option:`sphinx:sphinx-build -t`.
    """

    settings: Optional[Dict[str, Any]] = None
    """
    Override a configuration value set in the :file:`sphinx:conf.py` file.
    The value must be a number, string, list or dictionary value.

    For lists, you can separate elements with a comma like this: ``-D
    html_theme_path=path1,path2``.

    For dictionary values, supply the setting name and key like this:
    ``-D latex_elements.docclass=scrartcl``.

    For boolean values, use ``0`` or ``1`` as the value.

    See :option:`sphinx:sphinx-build -D`.
    """

    envvars: Optional[Dict[str, str]] = None
    """
    Inject environment variables during the build, which are available in
    the :file:`sphinx:conf.py` file.
    """

    html_values: Optional[Dict[str, str]] = None
    """
    Make the *name* assigned to *value* in the HTML templates.

    See :option:`sphinx:sphinx-build -A`.
    """

    nitpicky: bool = True
    """
    Run in nit-picky mode.  Currently, this generates warnings for all missing
    references.  See the config value :confval:`sphinx:nitpick_ignore` for a way
    to exclude some references as "known missing".

    See :option:`sphinx:sphinx-build -n`.
    """

    nocolor: bool = False
    """
    Do not emit colored output.

    See :option:`sphinx:sphinx-build -N`.
    """

    verbosity: int = 0
    """
    Increase verbosity (loglevel).  This option can be given up to **3**
    to get more debug logging output.  It implies :attr:`~traceback`.

    See :option:`sphinx:sphinx-build -v`.
    """

    quiet: bool = False
    """
    Do not output anything on standard output, only write warnings and
    errors to standard error.

    See :option:`sphinx:sphinx-build -q`.
    """

    very_quiet: bool = False
    """
    Do not output anything on standard output, also suppress warnings.
    Only errors are written to standard error.

    See :option:`sphinx:sphinx-build -Q`.
    """

    warnings_to_file: Optional[Path] = None
    """
    Write warnings (and errors) to the given file, in addition to standard error.

    See :option:`sphinx:sphinx-build -w`.
    """

    warning_are_errors: bool = False
    """
    Turn warnings into errors.  This means that the build stops at the first
    warning and the build is reported as failed.

    See :option:`sphinx:sphinx-build -W`.
    """

    keep_going: bool = False
    """
    With :attr:`~warnings_are_errors` option, keep going processing when
    getting warnings to the end of build, and the build is reported as failed.

    See :option:`sphinx:sphinx-build --keep-going`.
    """

    traceback: bool = False
    """
    Display the full traceback when an unhandled #information is saved to a file
    for further analysis.

    See :option:`sphinx:sphinx-build -T`.
    """

    run_debugger: bool = False
    """
    (Useful for debugging only.)  Run the Python debugger, :mod:`python:pdb`,
    if an unhandled exception occurs while building.

    See :option:`sphinx:sphinx-build -P`.
    """

    processes: Optional[Union[int, str]] = None
    """
    Distribute the build over *N* processes in parallel, to make building on
    multiprocessor machines more effective.  Note that not all parts and not all
    builders of Sphinx can be parallelized.  If ``auto`` argument is given,
    Sphinx uses the number of CPUs as *N*.

    See :option:`sphinx:sphinx-build -j`.
    """


class SphinxApidocOptions(BaseModel):
    src_root: Path
    module_first: bool = True
    separate: bool = False
    maxdepth: int = 4
    force: bool = True


class SphinxAutobuildOptions(SphinxBuildOptions):
    pass


class Sphinx:
    def __init__(
        self,
        project: SphinxProject,
        relative_to: Optional[Path] = None,
        sphinx_run: Optional[Callable[[List[str]], int]] = None,
        apidoc_run: Optional[Callable[[List[str]], int]] = None,
    ):
        self._project = project
        self._relative_to = relative_to
        self._sphinx = sphinx_run or get_sphinx()
        self._apidoc = apidoc_run or get_apidoc()

    def build_doc(
        self,
        options: SphinxBuildOptions,
        builder: str = "html",
    ) -> Path:
        destination_dir = self._resolve_path(self._project.build_root.joinpath(builder))

        args = self._make_sphinx_build_args(options, builder)

        args.append(str(self._resolve_path(self._project.doc_root)))
        args.append(str(destination_dir))

        environment_backup = os.environ.copy()
        try:
            additional_env = options.envvars or {}
            os.environ.update(additional_env)
            self._sphinx(args)
        finally:
            os.environ = environment_backup

        return destination_dir

    def autobuild(
        self,
        options: SphinxAutobuildOptions,
        builder: str = "html",
    ) -> Path:
        destination_dir = self._resolve_path(self._project.build_root.joinpath(builder))
        args = [
            sys.executable, "-m", "sphinx_autobuild"
        ]

        args.extend(self._make_sphinx_build_args(options, builder))
        args.append(str(self._resolve_path(self._project.doc_root)))
        args.append(str(destination_dir))

        additional_env = os.environ.copy()
        additional_env.update(options.envvars or {})
        subprocess.run(args, cwd=self._relative_to, env=additional_env)

        return destination_dir

    def build_apidoc(
        self,
        options: SphinxApidocOptions
    ) -> Path:
        args: List[str] = []
        if options.module_first:
            args.append("--module-first")
        if options.separate:
            args.append("--separate")
        if options.force:
            args.append("--force")
        args.extend(("--maxdepth", str(options.maxdepth)))
        args.extend(("--tocfile", "index", "--no-toc"))
        args.extend(("--output-dir", str(self._resolve_path(self._project.doc_root.joinpath("apidoc")))))

        args.append(str(self._resolve_path(options.src_root)))

        self._apidoc(args)

        return self._project.doc_root

    def _make_sphinx_build_args(
        self,
        options: SphinxBuildOptions,
        builder: str = "html",
    ) -> List[str]:
        tags = options.tags or []
        settings = options.settings or {}
        html_values = options.html_values or {}

        args: List[str] = [
            "-b",
            builder,
            "-d",
            str(self._resolve_path(self._project.build_root.joinpath("doctrees"))),
        ]

        if options.conf_dir:
            args.extend(("-c", str(self._resolve_path(options.conf_dir))))
        if options.write_all:
            args.append("-a")
        if options.rebuild_env:
            args.append("-E")
        if options.processes:
            args.extend(("-j", str(options.processes)))
        if options.nitpicky:
            args.append("-n")
        if options.nocolor:
            args.append("-N")
        if options.noconfig:
            args.append("-C")
        if options.quiet:
            args.append("-q")
        if options.very_quiet:
            args.append("-Q")
        if options.warnings_to_file:
            args.extend(("-w", str(self._resolve_path(options.warnings_to_file))))
        if options.warning_are_errors:
            args.append("-W")
        if options.keep_going:
            args.append("--keep-going")
        if options.traceback:
            args.append("-T")
        if options.run_debugger:
            args.append("-P")

        for tag in tags:
            args.extend(("-t", tag))

        for key, value in settings.items():
            if isinstance(value, Dict):
                for subkey, subvalue in value.items():
                    args.extend(("-D", f"{key}.{subkey}={subvalue}"))
            elif isinstance(value, List):
                args.extend(("-D", f"{key}={','.join(value)}"))
            elif isinstance(value, bool):
                boolstr = 1 if value else 0
                args.extend(("-D", f"{key}={boolstr}"))
            else:
                args.extend(("-D", f"{key}={value}"))

        for key, value in html_values.items():
            args.extend(("-A", f"{key}={value}"))

        return args

    def _resolve_path(self, path: Path):
        if self._relative_to:
            return self._relative_to.joinpath(path).resolve()
        else:
            return path.resolve()


class ProjectProfile(BaseModel):
    docs: SphinxProject
    target: str
    options: SphinxAutobuildOptions
    files: Optional[Dict[Path, Path]] = None
    apidocs: List[SphinxApidocOptions]
    check_links: bool = True

    @classmethod
    def default(cls):
        return cls(
            docs=SphinxProject(
                doc_root=Path("docs"),
                build_root=Path("build", "sphinx")
            ),
            target="html",
            options=SphinxAutobuildOptions(),
            files={},
            apidocs=[]
        )

    @property
    def built_path(self):
        return self.docs.build_root.joinpath(self.target)
