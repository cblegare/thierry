"""
A template for Thierry's picture.
"""
import shutil
from pathlib import Path
from typing import Dict, Optional

import pkg_resources

from outil.template import Template


class ThierryImage(Template):
    """
    Render Thierry's picture.
    """

    def render(
        self, parent_folder: Path, data: Optional[Dict[str, str]] = None
    ) -> Path:
        data = data or {}
        try:
            filename = data["filename"]
        except KeyError as e:
            raise ValueError("A filename is required") from e

        original_file = Path(
            pkg_resources.resource_filename(__package__, "thierry.png")
        )

        destination = parent_folder.joinpath(filename)

        shutil.copy(original_file, destination)
        return destination
