#!/bin/sh

###
# Install Nord colorschme for Konsole
curl -o $HOME/.local/share/konsole/nord.colorscheme \
    https://raw.githubusercontent.com/arcticicestudio/nord-konsole/develop/src/nord.colorscheme

###
# Install Oh My Zsh
#   See https://github.com/robbyrussell/oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

###
# Install Spaceship Oh My Zsh Scheme
#   See https://github.com/denysdovhan/spaceship-prompt
git clone https://github.com/denysdovhan/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt"
ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme"


###
# Install vim-plug for Neovim
#   See https://github.com/junegunn/vim-plug 
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
