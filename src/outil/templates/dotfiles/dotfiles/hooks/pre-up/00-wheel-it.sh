#!/bin/sh
###
# Prior to applying dotfiles, we need to make sure
# some things are installed, such as git, zsh, etc. 
###

echo "
Here are some package you way need to install.

# Python baseline
python3-pip
python3-venv

# Means to download stuff
git
curl

# Utilities for interactive work
tmux
zsh
neovim
xclip

# Compilation stuff
build-essential

# Codex and non-free goodies
kubuntu-restricted-extras
"
