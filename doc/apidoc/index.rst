###########################
Generated API documentation
###########################

The Outil application can be devided in the following sections:

**Services**

    Specific implementations, exposed as specialized library.
    They should not depend on the calling app, but the calling
    app most certainly will depend on them.

    See also

    *   :mod:`outil.template`

**Application protocols**

    Abstract Views, commands and other constructs used application
    wide to ensure a cohesive intercomponent communication.

    See also

    *   :mod:`outil.app.command`

    *   :mod:`outil.view`

**Interface**

    The only interface for |outil| is its command line parser.
    In case others are added in the future, it is good to note
    the the interface is responsible for booting the application
    and serving as a front controller for exposed commands.

    See also

    *   :mod:`outil.cli`

**Application**

    |outil| might offer multiple application implementation
    (one CLI and one Web interface, for instance).  The application
    wiring process can be dependent on configuration.  The fully
    setup application exposes all available services to commands.

    See also

    *   :mod:`outil.app`


.. toctree::
   :maxdepth: 4

   outil
   outil_theme
