.. _license:

=======
License
=======

outil and thierry - Opinionated utils for shell life-cycling.

Copyright (C) 2018  Charles Bouchard-Légaré


.. literalinclude:: ../LICENSE
    :language: none
